/**
  *@file atmel_avr_abstract.h
  *@brief Abstraction header for Atmel MCUs using the avr library
  *@author Jason Berger
  *@date 7/02/2024
  */


#pragma once

#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>

//Delay Abstraction
#define MRT_DELAY_MS(ms) _delay_ms(ms)

//Uart Abstraction
typedef FILE* mrt_uart_handle_t;
#define MRT_UART_TX(handle, data, len, timeout) fwrite(data, 1, len, handle)